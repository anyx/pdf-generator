import React from 'react';
import 'antd/dist/antd.css';
import styled from 'styled-components';
import AppLayout from './AppLayout';

const AppContainer = styled.div`
    width: 100%;
    height: 100%;
`;

function App() {
    return (
        <AppContainer>
            <AppLayout></AppLayout>
        </AppContainer>
    );
}

export default App;
