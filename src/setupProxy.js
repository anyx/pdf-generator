const proxy = require('http-proxy-middleware');

module.exports = function(app) {
    app.use('/api', proxy({
        target: 'http://test360-pdf.dev.experium.net',
        changeOrigin: true,
        pathRewrite: {
            '^/api': '/',
        },
    }));
};
