import React from 'react';
import { Icon, Upload } from 'antd';

const { Dragger } = Upload;

export default function FileUpload({ name, onUpload }) {
    const uploadProps = {
        name: 'file',
        fileList: [],
        transformFile(file) {
            return new Promise((resolve) => {
                const reader = new FileReader();
                reader.readAsText(file);
                reader.onload = () => {
                    resolve(reader.result);
                    onUpload(reader.result);
                };
            });
        },
    };

    return <Dragger {...uploadProps}>
        <p className="ant-upload-drag-icon">
            <Icon type="inbox" />
        </p>
        <p className="ant-upload-text">{name}</p>
        <p className="ant-upload-hint">
        </p>
    </Dragger>
}
