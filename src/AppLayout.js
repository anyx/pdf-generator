import React from 'react';
import { Layout } from 'antd';
import PdfForm from './PdfForm';
import styled from 'styled-components';

const AppHeader = styled.div`
    color: #ffffff;
    text-align: center;
    font-size: 26px;
`;

export default function AppLayout() {
    const initialTask = {
        'cover': '',
        'header': '',
        'body': '',
        'footer': '',
        'last': '',
        'toc': false,
        'fileName': 'my-pdf',
        'notifyUrl': '',
        'coverOptions': {
            'orientation': 'landscape',
            'marginTop': 0,
            'marginBottom': 0,
            'pageHeight': '254mm',
            'pageWidth': '143mm',
            'marginLeft': 0,
            'marginRight': 0,
            'disableSmartShrinking': false,
            'dpi': false
        },
        'options': {
            'orientation': 'landscape',
            'marginTop': '10mm',
            'marginBottom': '8mm',
            'pageHeight': '254mm',
            'pageWidth': '143mm',
            'marginLeft': 0,
            'marginRight': 0,
            'disableSmartShrinking': false,
            'dpi': false
        },
        'endingFileUrl': ''
    };

    return <Layout style={{ height: '100%' }}>
        <Layout.Header className="header">
            <AppHeader>Генератор pdf</AppHeader>
        </Layout.Header>
        <Layout.Content style={{ padding: '24px' }}>
            <Layout.Content style={{
                background: '#fff',
                padding: 24,
                margin: 0,
                minHeight: 280,
            }}>
                <PdfForm initialTask={initialTask}/>
            </Layout.Content>
        </Layout.Content>
    </Layout>
}
