import React, { useState } from 'react';
import styled from 'styled-components';
import { Button, Col, Form, Icon, message, Row, Spin, Result } from 'antd';
import ReactJson from 'react-json-view'
import FileUpload from './FileUpload';
import useInterval from './useInterval';

const formItemLayout = {};

const Container = styled.div`
    margin: 16px 0;
`;

const url = '/api';

const fileFields = [
    'cover',
    'header',
    'body',
    'footer',
    'last',
];

const PdfForm = ({ initialTask }) => {
    const [task, setTask] = useState(initialTask);
    const [sending, setSending] = useState(false);
    const [sendingResult, setSendingResult] = useState({});
    const [waitingGeneration, setWaitingGeneration] = useState(false);
    const [updateRequestPending, setUpdateRequestPending] = useState(false);
    const [generationResult, setGenerationResult] = useState(false);
    const [taskStatus, setTaskStatus] = useState({});

    useInterval(async () => {
        if (!waitingGeneration || updateRequestPending || !sendingResult._id) {
            return false;
        }

        setUpdateRequestPending(true);

        const response = await fetch(`${url}/${sendingResult._id}`, {
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
            },
        });

        const result = await response.json();

        setTaskStatus(result);
        setUpdateRequestPending(false);

        if (result.status === 'complete') {
            setWaitingGeneration(false);
            setGenerationResult(result.fileUrl);
        }

    }, 2000);

    const send = async () => {
        setSending(true);
        setSendingResult({});
        setWaitingGeneration(false);
        setGenerationResult(null);
        setUpdateRequestPending(false);
        setTaskStatus({});

        try {
            const response = await fetch(url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json;charset=utf-8',
                },
                body: JSON.stringify(task)
            });

            const result = await response.json();

            setSendingResult(result);
            setWaitingGeneration(response.ok);
        } catch (e) {
            message.error('Не удалось отправить запрос');
            setSendingResult(e);
            setWaitingGeneration(false);
        } finally {
            setSending(false);
        }
    };

    const setUploadValue = (code, value) => {
        setTask({ ...task, [code]: value });
    };

    const antIcon = <Icon type='loading' style={{ fontSize: 24 }} spin/>;

    return (
        <Row gutter={16}>
            <Col span={12}>
                <fieldset>
                    <legend>Параметры</legend>
                    <Form  {...formItemLayout}>
                        <Row gutter={16}>
                            {fileFields.map((field) => (
                                    <Col span={4} key={`file-${field}`}>
                                        <FileUpload name={field} onUpload={(value) => setUploadValue(field, value)}/>
                                    </Col>
                                )
                            )}
                        </Row>
                    </Form>
                    <Container>
                        <Button disabled={sending} onClick={send} type='primary' loading={sending}>Отправить</Button>
                    </Container>

                    {Object.keys(sendingResult).length > 0 && <Container>
                        <div>Результат отправки:</div>
                        <ReactJson name={null} src={sendingResult} displayDataTypes={false}/>
                    </Container>}

                    {waitingGeneration && <Container>
                        <Spin indicator={antIcon}/> Ожидаем генерации...
                    </Container>}

                    {Object.keys(taskStatus).length > 0 && <Container>
                        <div>Статус задачи:</div>
                        <ReactJson name={null} collapsed={true} src={taskStatus} displayDataTypes={false} collapseStringsAfterLength={50}/>
                    </Container>}

                    {generationResult && <Container>
                        <Result
                            status='success'
                            title='Файл'
                            extra={
                                <a href={generationResult} target='_blank' rel='noopener noreferrer'>{generationResult}</a>
                            }
                        />,
                    </Container>}

                </fieldset>
            </Col>
            <Col span={12}>
                <fieldset>
                    <legend>Тело запроса</legend>
                    <ReactJson
                        src={task}
                        name={null}a
                        theme='railscasts'
                        collapseStringsAfterLength={50}
                        onEdit={({ updated_src }) => {
                            setTask(updated_src);
                            return true;
                        }}
                        onAdd={() => true}
                        onDelete={() => true}
                    />
                </fieldset>
            </Col>
        </Row>
    )
};

export default PdfForm;
